package presentation;

import dataAccessLayer.Repository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Costumer;

public class ControllerCostumers implements Initializable {
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button editButton;
    @FXML
    private Button findButton;
    @FXML
    private TextField idField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField addressField;
    @FXML
    private TextField emailField;
    @FXML
    private TextField phoneNumberField;
    @FXML
    private TableView<Costumer> costumersTable;
    @FXML
    private TableColumn<Costumer, Integer> idColumn;
    @FXML
    private TableColumn<Costumer, String> nameColumn;
    @FXML
    private TableColumn<Costumer, String> addressColumn;
    @FXML
    private TableColumn<Costumer, String> emailColumn;
    @FXML
    private TableColumn<Costumer, String> phoneNumberColumn;

    private ObservableList<Costumer> data;


    public void initialize(URL location, ResourceBundle resources) {
        data = FXCollections.observableArrayList(Repository.getData(Costumer.class));
        idColumn.setCellValueFactory(new PropertyValueFactory<Costumer, Integer>("idCostumer"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("name"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("address"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("email"));
        phoneNumberColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("phoneNumber"));
        costumersTable.setItems(data);
    }

    public void insertClient() {
        Costumer costumer = new Costumer(Integer.parseInt(idField.getText()), nameField.getText(), addressField.getText(), emailField.getText(), phoneNumberField.getText());
        Repository.insert(costumer);

        data = FXCollections.observableArrayList(Repository.getData(Costumer.class));
        costumersTable.setItems(data);
    }

    public void deleteClient() {
        Costumer costumer = costumersTable.getSelectionModel().getSelectedItem();
        Repository.delete(Costumer.class, costumer.getIdCostumer());

        data = FXCollections.observableArrayList(Repository.getData(Costumer.class));
        costumersTable.setItems(data);
    }

//    public void editClient() {
//        Costumer costumer = costumersTable.getSelectionModel().getSelectedItem();
//        Repository.update(Costumer.class, costumer.getIdCostumer());
//
//        data = FXCollections.observableArrayList(Repository.getData(Costumer.class));
//        costumersTable.setItems(data);
//    }

}
