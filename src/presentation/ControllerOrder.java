package presentation;

import buisinessLayer.OrderProcessing;
import dataAccessLayer.Repository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Costumer;
import model.Order;
import model.OrderItem;
import model.Product;

import java.net.URL;
import java.util.ResourceBundle;

public class ControllerOrder implements Initializable {

    private static Integer idOrder = 0;
    private static Integer idOrderItem = 0;

    @FXML
    private TableView<Costumer> costumersTable;
    @FXML
    private TableColumn<Costumer, Integer> idColumnC;
    @FXML
    private TableColumn<Costumer, String> nameColumnC;
    @FXML
    private TableColumn<Costumer, String> addressColumn;
    @FXML
    private TableColumn<Costumer, String> emailColumn;
    @FXML
    private TableColumn<Costumer, String> phoneNumberColumn;

    private ObservableList<Costumer> dataC;


    @FXML
    private TableView<Product> productsTable;
    @FXML
    private TableColumn<Product, Integer> idColumn;
    @FXML
    private TableColumn<Product, String> nameColumn;
    @FXML
    private TableColumn<Product, Double> priceColumn;
    @FXML
    private TableColumn<Product, Integer> stockColumn;

    private ObservableList<Product> data;

    @FXML
    private TextField idClientField;
    @FXML
    private TextField quantityField;

    public void initialize(URL location, ResourceBundle resources) {
        dataC = FXCollections.observableArrayList(Repository.getData(Costumer.class));
        idColumnC.setCellValueFactory(new PropertyValueFactory<Costumer, Integer>("idCostumer"));
        nameColumnC.setCellValueFactory(new PropertyValueFactory<Costumer, String>("name"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("address"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("email"));
        phoneNumberColumn.setCellValueFactory(new PropertyValueFactory<Costumer, String>("phoneNumber"));
        costumersTable.setItems(dataC);

        data = FXCollections.observableArrayList(Repository.getData(Product.class));
        idColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("idProduct"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
        stockColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
        productsTable.setItems(data);
    }

    public void addOrderItem() {
        idOrderItem++;
        idOrder++;
        Product product = productsTable.getSelectionModel().getSelectedItem();
        Integer idClient = Integer.parseInt(idClientField.getText());
        Integer quantity = Integer.parseInt(quantityField.getText());
        Order order = new Order(idOrder, idClient, quantity * product.getPrice());
        OrderItem orderItem = new OrderItem(idOrderItem, order.getIdOrder(), product.getIdProduct(), quantity);

        product.setStock(product.getStock() - quantity);

        OrderProcessing.createReceipt(idClient, order);
        Repository.insert(order);
        Repository.insert(orderItem);

        data = FXCollections.observableArrayList(Repository.getData(Product.class));
        productsTable.setItems(data);
    }


}
