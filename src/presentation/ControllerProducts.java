package presentation;


import dataAccessLayer.Repository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.awt.*;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import model.Product;

public class ControllerProducts implements Initializable{
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button editButton;
    @FXML
    private Button findButton;
    @FXML
    private TextField idField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField stockField;
    @FXML
    private TableView<Product> productsTable;
    @FXML
    private TableColumn<Product, Integer> idColumn;
    @FXML
    private TableColumn<Product, String> nameColumn;
    @FXML
    private TableColumn<Product, Double> priceColumn;
    @FXML
    private TableColumn<Product, Integer> stockColumn;

    private ObservableList<Product> data;

    public void initialize(URL location, ResourceBundle resources) {
        data = FXCollections.observableArrayList(Repository.getData(Product.class));
        idColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("idProduct"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));
        stockColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
        productsTable.setItems(data);
    }

    public void insertProduct() {
        Product product = new Product(Integer.parseInt(idField.getText()), nameField.getText(), Double.parseDouble(priceField.getText()), Integer.parseInt(stockField.getText()));
        Repository.insert(product);

        data = FXCollections.observableArrayList(Repository.getData(Product.class));
        productsTable.setItems(data);
    }

    public void deleteProduct() {
        Product product = productsTable.getSelectionModel().getSelectedItem();
        Repository.delete(product.getClass(),product.getIdProduct());

        data = FXCollections.observableArrayList(Repository.getData(Product.class));
        productsTable.setItems(data);
    }

//    public void editProduct() {
//        Product product = productsTable.getSelectionModel().getSelectedItem();
//        Repository.update(product, Product.class, product.getIdProduct());
//
//        data = FXCollections.observableArrayList(Repository.getData(Product.class));
//        productsTable.setItems(data);
//    }

}
