package model;

public class Order {
    private Integer idOrder;
    private Integer idClient;
    private Double value;

    public Order(Integer idOrder, Integer idClient, Double value) {
        this.idOrder = idOrder;
        this.idClient = idClient;
        this.value = value;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Integer getIdOrder() {
        return this.idOrder;
    }

    public void setIdClient(Integer idClient) {
        this.idClient = idClient;
    }

    public Integer getIdClient() {
        return this.idClient;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getValue() {
        return this.value;
    }
}
