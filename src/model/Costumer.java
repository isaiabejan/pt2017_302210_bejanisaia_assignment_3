package model;

public class Costumer {
    private Integer idCostumer;
    private String name;
    private String address;
    private String email;
    private String phoneNumber;

    public Costumer (Integer idCostumer, String name, String address, String email, String phoneNumber) {
        this. idCostumer = idCostumer;
        this.name = name;
        this. address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Costumer() {

    }

    public void setIdCostumer(Integer idCostumer) {
        this.idCostumer = idCostumer;
    }

    public Integer getIdCostumer() {
        return this.idCostumer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return this.address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return this.email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }
}
