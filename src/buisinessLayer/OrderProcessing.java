package buisinessLayer;

import model.Costumer;
import model.Order;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class OrderProcessing {

    public static void createReceipt(Integer idCostumer, Order order) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("Receipt" + idCostumer + ".txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.println("Order number: " + order.getIdOrder() + "\tCostumer ID: " + idCostumer + "\tValue: " + order.getValue());
        writer.close();
    }
}
