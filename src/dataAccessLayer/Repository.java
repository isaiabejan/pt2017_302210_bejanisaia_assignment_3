package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Repository {
    private String query;

    public static void insert(Object object) {

        StringBuilder insertStatement = new StringBuilder("INSERT INTO schooldb." + object.getClass().getSimpleName() + " (");

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;


        int comma = 0;

        for(Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                if(comma == 1) {
                    insertStatement.append(", " + field.getName());
                } else {
                    comma = 1;
                    insertStatement.append(field.getName());
                }
            } catch(IllegalArgumentException e) {
                e.printStackTrace();
            }
        }

        insertStatement.append(") VALUES (");
        comma = 0;

        for(Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);

            try {
                if(comma == 1) {
                    insertStatement.append(", '" + field.get(object) + "'");
                } else {
                    comma = 1;
                    insertStatement.append("'" + field.get(object) + "'");
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        insertStatement.append(")");

        try {
            statement = conn.prepareStatement(insertStatement.toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
//            ConnectionFactory.close(statement);
//            ConnectionFactory.close(conn);
        }
    }

    public static <T> void delete(Class<T> clazz, Integer id) {
        StringBuilder deleteStatement = new StringBuilder("DELETE FROM " + clazz.getSimpleName() + " WHERE ");

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;

        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);

            if (field.getName().contains("id")) {
                try {
                    deleteStatement.append(field.getName() + " = '" + id + "'");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        try {
            statement = conn.prepareStatement(deleteStatement.toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
//            ConnectionFactory.close(statement);
//            ConnectionFactory.close(conn);
        }
    }

    public static <T> List<T> getData(Class<T> clazz) {

        List<T> data = new ArrayList<T>();
        String selectStatement = "SELECT * FROM " + clazz.getSimpleName();

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            statement = conn.prepareStatement(selectStatement);
            rs = statement.executeQuery();
            while (rs.next()) {
                T instance = clazz.newInstance();
                for(Field field : clazz.getDeclaredFields()) {
                    Object value = rs.getObject(field.getName());
                    if (field.getName().equals("price")) {
                        value = Double.parseDouble((String)value);
                    }
                    //T value = (T) rs.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), clazz);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                data.add(instance);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
//            ConnectionFactory.close(rs);
//            ConnectionFactory.close(statement);
//            ConnectionFactory.close(conn);
        }

        return data;
    }

    public static <T> void update(Object object, Class<T> clazz, Integer id) {
        StringBuilder updateStatement = new StringBuilder("UPDATE " + clazz.getSimpleName() + " SET ");

        Connection conn = ConnectionFactory.getConnection();
        PreparedStatement statement = null;

        int comma = 0;

        for(Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);

            if(comma == 1) {
                updateStatement.append(", " + field.getName() + "=?");
            } else {
                comma = 1;
                updateStatement.append(field.getName() + "=?");
            }
        }

        for(Field field : clazz.getDeclaredFields()) {
            if(field.getName().contains("id")) {
                updateStatement.append(" WHERE " + field.getName() + " = " + id);
                break;
            }
        }

        try {
            statement = conn.prepareStatement(updateStatement.toString());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
